<?php

namespace laylatichy\nano\core\attributes\middleware;

use Attribute;
use Exception;
use InvalidArgumentException;
use laylatichy\Nano;
use laylatichy\nano\core\MiddlewareModule;
use ReflectionClass;
use ReflectionMethod;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
final class Middleware {
    public static function get(ReflectionClass|ReflectionMethod $reflection): array {
        $middleware = [];
        $attributes = $reflection->getAttributes(name: self::class);

        foreach ($attributes as $attribute) {
            if (isset($attribute->getArguments()[0], $attribute->getArguments()[1])) {
                try {
                    $m            = new MiddlewareModule();
                    $m->class     = new ReflectionClass($attribute->getArguments()[0]);
                    $m->method    = $m->class->getMethod($attribute->getArguments()[1]);
                    $middleware[] = $m;
                } catch (Exception $e) {
                    throw new InvalidArgumentException(
                        message: $e->getMessage()
                    );
                }
            } else {
                throw new InvalidArgumentException(
                    message: 'invalid argument for ' . __CLASS__ . ' attribute'
                );
            }
        }

        return $middleware;
    }

    public static function getCallbacks(array $middleware, Nano $nano, array $args): array {
        $filtered = self::filter($middleware);

        /** @var MiddlewareModule $m */
        foreach ($filtered as $m) {
            $m->callback = fn ($request, ...$args) => $m->method->invoke(
                $m->class->newInstanceWithoutConstructor(),
                $nano,
                ...$args,
            );
        }

        return $filtered;
    }

    private static function filter(array $middleware): array {
        $known  = [];
        $unique = [];

        return array_filter($middleware, function (MiddlewareModule $m) use (&$known) {
            $unique  = !in_array($m->method->class . $m->method->name, $known);
            $known[] = $m->method->class . $m->method->name;

            return $unique;
        });
    }
}
