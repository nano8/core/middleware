<?php

namespace laylatichy\nano\core;

use ReflectionClass;
use ReflectionMethod;

final class MiddlewareModule {
    public ReflectionClass   $class;

    public ReflectionMethod $method;

    public object            $callback;
}
