<?php

namespace laylatichy\nano\core;

use laylatichy\Nano;
use laylatichy\nano\core\attributes\helpers\RequirePOST;
use laylatichy\nano\core\attributes\middleware\Middleware as MiddlewareAttribute;
use laylatichy\nano\core\attributes\router\BASE;
use laylatichy\NanoController;
use ReflectionClass;
use ReflectionMethod;

final class Middleware {
    public function __construct(
        private array $_base = [],
        private array $_routes = [],
        private array $_required = [],
    ) {
    }

    public function init(Nano $nano): void {
        $classes = array_filter(get_declared_classes(), fn ($class) => is_subclass_of(
            object_or_class: $class,
            class: NanoController::class
        ));
        foreach ($classes as $class) {
            $reflection = new ReflectionClass(objectOrClass: $class);
            $caller     = $reflection->newInstanceWithoutConstructor();

            $routeBase      = BASE::get(reflection: $reflection);
            $middlewareBase = MiddlewareAttribute::get(reflection: $reflection);

            foreach ($reflection->getMethods() as $method) {
                $args = $this->getMethodArgs(method: $method);

                $required = RequirePOST::get(reflection: $method, nano: $nano);

                $middlewareMethod = MiddlewareAttribute::get(reflection: $method);

                $middleware = MiddlewareAttribute::getCallbacks(
                    middleware: [...$middlewareBase, ...$middlewareMethod],
                    nano: $nano,
                    args: $args
                );

                Base::generateRoutes(
                    caller: $caller,
                    reflection: $method,
                    base: $routeBase,
                    middleware: $middleware,
                    args: $args,
                    nano: $nano,
                    required: $required,
                );
            }
        }
    }

    public function getAllBase(): array {
        return $this->_base;
    }

    public function addBase(object $callback): void {
        $this->_base[] = $callback;
    }

    public function getAllRoutes(): array {
        return $this->_routes;
    }

    public function getSingleRoutes(string $path): array {
        return $this->_routes[$path] ?? [];
    }

    public function addRoutes(string $path, MiddlewareModule $middleware): void {
        $this->_routes[$path][] = $middleware;
    }

    public function getAllRequired(): array {
        return $this->_required;
    }

    public function getSingleRequired(string $path): array {
        return $this->_required[$path] ?? [];
    }

    public function addRequired(string $path, MiddlewareModule $middleware): void {
        $this->_required[$path][] = $middleware;
    }

    private function getMethodArgs(ReflectionMethod $method): array {
        $params = $method->getParameters();
        array_shift($params);

        return array_map(fn ($p) => '$' . $p->getName(), $params);
    }
}
