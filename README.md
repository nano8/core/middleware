### nano/core/middleware

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-middleware?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-middleware)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-middleware?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-middleware)

##### CI STATUS

- dev [![pipeline status](https://gitlab.com/nano8/core/middleware/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/middleware/-/commits/dev)
- master [![pipeline status](https://gitlab.com/nano8/core/middleware/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/middleware/-/commits/master)
- release [![pipeline status](https://gitlab.com/nano8/core/middleware/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/middleware/-/commits/release)

#### Install

- `composer require laylatichy/nano-core-middleware`
